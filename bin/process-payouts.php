<?php

require __DIR__ . "/../bootstrap.php";

$many = [
	"BTC" => [],
	"LTC" => [],
	"DOGE" => [],
];
$payouts = Payout::getUnprocessed();
foreach ($payouts as $payout) {
	$many[$payout->currency][$payout->toAddr] = $payout->amount;
}

foreach ($many as $currency => $m) {
	if (empty($m)) {
		continue;
	}
	$client = Blockchain::getInstance($currency);
	$client->sendmany($m);
}