<?php

require __DIR__ . "/../bootstrap.php";

// check required parameters
if (!isset($argv[1])) {
	fprintf(STDERR, DATETIME . "\tBAD\tMissing daemon id\n");
	exit(-1);
}
if (!isset($argv[2])) {
	fprintf(STDERR, DATETIME . "\tBAD\tMissing txid\n");
	exit(-2);
}

// parse parameters
$currency = $argv[1];
$txid = $argv[2];
fprintf(STDOUT, DATETIME . "\tINIT\t$currency $txid\n");

// process incoming transaction
$processor = new Processor($currency);
try {
	$tips = $processor->receive($txid);
} catch (Exception $e) {
	fprintf(STDERR, DATETIME . "\tBAD\t{$txid}\t" . $e->getMessage() . "\n");
	exit(-3);
}

// log processed tips
foreach ($tips as $tip) {
	fprintf(STDOUT, DATETIME . "\tOK\t{$txid}\t" . json_encode($tip) . "\n");
}
