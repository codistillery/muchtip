<?php

require __DIR__ . "/../bootstrap.php";

// parse parameters
if (!isset($argv[1])) {
	fprintf(STDERR, DATETIME . "\tBAD\tMissing currency\n");
	exit(-1);
}
$currency = $argv[1];

// leave mark in logs
fprintf(STDOUT, DATETIME . "\tINIT\t$currency\n");

// fetch payouts
$payouts = Payout::getUnprocessed($currency);
if (empty($payouts)) {
	fprintf(STDOUT, DATETIME . "\tOK\t{$currency}\tNothing to process\n");
	exit(0);
}

// process incoming transaction
$processor = new Processor($currency);
try {
	$out = $processor->send($payouts);
} catch (Exception $e) {
	fprintf(STDERR, DATETIME . "\tBAD\t{$currency}\t" . $e->getMessage() . "\n");
	exit(-2);
}

// log processed payouts
if ($processed = $out["processed"]) {
	fprintf(STDOUT, DATETIME . "\tOK\t{$currency}\tProcessed {$processed} payouts\n");
}
if (!empty($out["failed"])) {
	fprintf(STDERR, DATETIME . "\tBAD\t{$currency}\tFailed\t" . json_encode($out["failed"]) . "\n");
	exit(-3);
}
