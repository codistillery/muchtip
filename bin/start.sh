#!/bin/bash

nice -n 20 bitcoind -walletnotify="./txn-hook.sh btc %s" &
nice -n 20 litecoind -walletnotify="./txn-hook.sh ltc %s" &
nice -n 20 dogecoind -walletnotify="./txn-hook.sh doge %s" &

tail -f ../var/log/receive.log
