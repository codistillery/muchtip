<?php

// environment name (create file "env" with environment name in it)
define("ENV", trim(file_get_contents(__DIR__ . "/env")));

require __DIR__ . "/vendor/autoload.php";

require __DIR__ . "/lib/Blockchain.php";
require __DIR__ . "/lib/Config.php";
require __DIR__ . "/lib/DB.php";
require __DIR__ . "/lib/Dispatcher.php";
require __DIR__ . "/lib/PDOHelper.php";
require __DIR__ . "/lib/Presenter.php";
require __DIR__ . "/lib/PresenterJson.php";
require __DIR__ . "/lib/PresenterPhtml.php";
require __DIR__ . "/lib/PresenterTwig.php";
require __DIR__ . "/lib/RequestHelper.php";
require __DIR__ . "/lib/RestService.php";
require __DIR__ . "/lib/ServiceLocator.php";
require __DIR__ . "/lib/Storage.php";
require __DIR__ . "/lib/TwigFactory.php";

require __DIR__ . "/model/Jar.php";
require __DIR__ . "/model/Source.php";
require __DIR__ . "/model/Tip.php";
require __DIR__ . "/model/Payout.php";
require __DIR__ . "/model/Processor.php";

// set up class autoloader
spl_autoload_register(static function ($className) {
	$f = realpath(__DIR__) . "/../lib/{$className}.php";
	if (file_exists($f)) {
		include_once $f;
	}
});

// instantiate config
$config = Config::getInstance();

// handy consts
define("NOW", time());
define("DATETIME", date("Y-m-d H:i:s"));
define("JAR_TIMEOUT", 3600 * 24 * 7); // one week

define("BASE_URL", $config->location->base);
define("FEE_SHARE", $config->shares->fee);
define("AUTHORS_SHARE", $config->shares->author);

define("QR_CODE_PATH", realpath(__DIR__ . "/public/var/qr"));
define("QR_CODE_URL", "var/qr/");

define("TWIG_CACHE_DIR", realpath(__DIR__ . "/var/twig-cache/"));
define("TEMPLATES_DIR", realpath(__DIR__ . "/templates") . "/");

// set up database connection
DB::configure($config->db->database, $config->db->username, $config->db->password, $config->db->host, $config->db->port);
DB::setDefault($config->db->database);
