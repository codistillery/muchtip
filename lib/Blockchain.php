<?php

/**
 * Blockchain daemon JSON-RPC client and connections pool
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Blockchain {
	
	protected static $instances = [];

	protected $currency;
	protected $endpoint;
	protected $client;

	/**
	 * Constructor
	 *
	 * @param string $currency
	 */
	protected function __construct($currency, $endpoint) {
		$this->currency = $currency;
		$this->endpoint = $endpoint;
	}

	/**
	 * Return blockchain client instance
	 *
	 * @param string $currency
	 * @return Blockchain
	 */
	public static function getClient($currency) {
		if (!isset(self::$instances[$currency])) {
			$config = Config::getInstance();
			self::$instances[$currency] = new Blockchain($currency, $config->daemon->$currency);
		}
		return self::$instances[$currency];
	}

	/**
	 * Make call to Bitcoind
	 *
	 * @param string $method
	 * @param array $args
	 * @return mixed
	 */
	public function __call($method, $args) {
		require_once __DIR__ . "/../vendor/jsonrpcphp/includes/jsonRPCClient.php";
		$this->client or $this->client = new jsonRPCClient($this->endpoint);
		return call_user_func_array([$this->client, $method], $args);
	}

	/**
	 * Retrieve sending address from transaction
	 *
	 * @param string $txid
	 * @return string
	 */
	public function findFromAddress($txid) {
		$fromAddr = null;
		$raw = $this->client->decoderawtransaction($this->client->getrawtransaction($txid));
		foreach ($raw["vin"] as $input) {
			$inputRaw = $this->client->decoderawtransaction($this->client->getrawtransaction($input["txid"]));
			$fromAddr = $inputRaw["vout"][$input["vout"]]["scriptPubKey"]["addresses"][0]; // http://bitcoin.stackexchange.com/a/5981/7104
			// TODO log "Found input address $fromAddr"
			break;
		}
		reset($raw); // move iterator pointer back to beginning
		return $fromAddr;
	}

}