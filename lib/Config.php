<?php

/**
 * Configuration container
 *
 * @author Michał Rudnicki <michal.rudnicki@telefonica.com>
 */
final class Config implements IteratorAggregate {

	/**
	 * Configuration directory
	 */
	const DIR = "../conf";

	const ENV_PRODUCTION = "production";
	const ENV_PREPRODUCTION = "preproduction";

	/**
	 * Configuration node value
	 * @var scalar
	 */
	protected $value = null;

	/**
	 * Configuration subnodes
	 * @var Config[]
	 */
	protected $nodes;

	/**
	 * Singleton instance
	 * @var Config
	 */
	protected static $instance;

	/**
	 * Constructor
	 */
	protected function __construct($value = null) {
		$this->value = $value;
		$this->nodes = new ArrayObject();
	}

	/**
	 * Return singleton instance
	 *
	 * @return Config
	 */
	public static function getInstance() {
		if (null === self::$instance) {
			self::$instance = self::load(__DIR__ . DIRECTORY_SEPARATOR . self::DIR . DIRECTORY_SEPARATOR . ENV . ".ini");
			if (self::$instance->env !== ENV) {
				throw new ConfigException("Environment name in ini file does not match predefined ENV const");
			}
		}
		return self::$instance;
	}

	/**
	 * Return config loaded from file
	 *
	 * @param string $file
	 * @return Config
	 */
	protected static function load($file) {
		if (!file_exists($file)) {
			throw new ConfigException("Config file $file not found");
		}

		$config = new Config();
		$ini = parse_ini_file($file, false, INI_SCANNER_RAW);
		foreach ($ini as $key => $value) {
			$config->install($key, $value);
		}
		return $config;
	}

	/**
	 * Mock config loaded from ini array
	 *
	 * @param array $ini
	 * @return Config
	 */
	public static function mock(array $ini) {
		$config = new Config();
		foreach ($ini as $key => $value) {
			$config->install($key, $value);
		}
		return $config;
	}

	/**
	 * Install key/value pairs as value or subnodes
	 *
	 * @param string $key
	 * @param string $value
	 */
	protected function install($key, $value) {
		$dot = strpos($key, ".");
		if (false === $dot) {
			$this->nodes[$key] = new Config($value);
		} else {
			$head = substr($key, 0, $dot);
			$tail = substr($key, $dot + 1);
			isset($this->nodes[$head]) or $this->nodes[$head] = new Config();
			$this->nodes[$head]->install($tail, $value);
		}
	}

	/**
	 * Property getter
	 *
	 * If node exists and has no subnodes scalar value is returned, otherwise Config object.
	 *
	 * @return scalar|Config
	 */
	public function __get($property) {
		if (!isset($this->nodes[$property])) {
			return new Config();
		}
		if (0 === $this->nodes[$property]->nodes->count()) {
			return $this->nodes[$property]->value;
		}
		return $this->nodes[$property];
	}

	public function getValue() {
		return $this->value;
	}

	public function hasSubNodes() {
		return 0 !== count($this->nodes);
	}

	/**
	 * Indicate whether on production
	 *
	 * @return boolean
	 */
	public function isProduction() {
		return self::ENV_PRODUCTION === $this->env;
	}

	/**
	 * Indicate whether on preproduction
	 *
	 * @return boolean
	 */
	public function isPreproduction() {
		return self::ENV_PREPRODUCTION === $this->env;
	}

	/**
	 * Indicate whether on development
	 *
	 * @return boolean
	 */
	public function isDevelopment() {
		return self::ENV_PRODUCTION !== $this->env and self::ENV_PREPRODUCTION !== $this->env;
	}

	/**
	 * Return property value as string
	 *
	 * @return string
	 */
	public function __toString() {
		return (string)$this->value;
	}

	/**
	 * Return subnodes iterator
	 *
	 * @return Iterator
	 */
	public function getIterator() {
		return $this->nodes->getIterator();
	}

}

final class ConfigException extends Exception { }