<?php
/**
 * Database connection (PDO) pool
 *
 * @author Michał Rudnicki <michal.rudnicki@telefonica.com>
 */
final class DB {

	/**
	 * Connections pool
	 * @var array
	 */
	protected static $connections = array();

	/**
	 * Default database name
	 * @var string
	 */
	protected static $default;

	/**
	 * Private constructor
	 */
	protected function __construct() { }

	/**
	 * Configure database connection
	 *
	 * @param string $database
	 * @param string $username
	 * @param string $password
	 * @param null   $host
	 * @param null   $port
	 */
	public static function configure($database, $username, $password, $host = null, $port = null) {
		self::$connections[$database] = array(
			"username" => $username,
			"password" => $password,
			"host" => $host,
			"port" => $port,
			"pdo" => null,
		);
	}

	public static function setDefault($database) {
		if (!isset(self::$connections[$database])) {
			throw new Exception("No definition for database $database");
		}
		self::$default = $database;
	}

	/**
	 * Return instance of PDO
	 *
	 * Connection parameters are taken from global defines: DB_host, DB_NAME, DB_PASSWORD.
	 * PDO error reporting is set to use exceptions.
	 * Fetch mode is set to use hash arrays.
	 *
	 * @param string $database
	 * @return PDO
	 * @throws Exception
	 * @throws PDOException
	 */
	public static function get($database = null) {
		if (null === $database and !self::$default) {
			throw new Exception("Default database not set");
		}
		$database or $database = self::$default;
		if (!isset(self::$connections[$database])) {
			throw new Exception("No definition for database $database");
		}
		if (!self::$connections[$database]["pdo"]) {
			$dsn = "mysql:dbname={$database};charset=utf8";
			self::$connections[$database]["host"] and $dsn .= ";host=" . self::$connections[$database]["host"];
			self::$connections[$database]["port"] and $dsn .= ";port=" . self::$connections[$database]["port"];
			$username = self::$connections[$database]["username"];
			$password = self::$connections[$database]["password"];
			$options = array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			);
			try {
				self::$connections[$database]["pdo"] = new PDO($dsn, $username, $password, $options);
			} catch (PDOException $e) {
				echo "The database is currently off line. Please come back later.";
				die();
			}
		}
		return self::$connections[$database]["pdo"];
	}

}