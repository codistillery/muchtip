<?php

/**
 * Dispatches requests to service class
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Dispatcher {

	protected $serviceName;
	protected $actionName;
	protected $format;
	protected $id;

	protected $httpStatusCode;
	protected $location;

	/**
	 * Constructor
	 */
	public function __construct() { }

	/**
	 * Configure dispatcher with service name and action
	 *
	 * @param string $serviceName
	 * @param string $actionName
	 * @return Dispatcher
	 */
	public function configure($serviceName, $actionName, $id = null) {
		$this->serviceName = $serviceName;
		$this->actionName = strtolower($actionName);
		$this->id = $id;
		return $this;
	}

	/**
	 * Configure dispatcher with service name and action taken from request
	 *
	 * @return Dispatcher
	 */
	public function configureFromRequest() {
		$path = ltrim(RequestHelper::path("/home.html"), "/");
		($pos = strpos($path, "?")) and $path = substr($path, 0, $pos);
		$format = ($pos = strpos($path, ".")) ? substr($path, $pos + 1) : "html";
		$pos and $path = substr($path, 0, -(strlen($format) + 1));
		$segments = explode("/", $path);
		$service = array_shift($segments);
		$service or $service = "home";
		$id = empty($segments) ? null : array_shift($segments);
		is_null($id) and $id = RequestHelper::request("id");

		// set service parameters
		$this->id = $id;
		$this->format = $format;
		$this->serviceName = ucfirst($service) . "Service";
		$a = strtolower(RequestHelper::method());
		$this->actionName = ("get" === $a and !$id) ? "index" : strtolower($a);
		return $this;
	}

	/**
	 * Return service name
	 *
	 * @return string
	 */
	public function getServiceName() {
		return $this->serviceName;
	}

	/**
	 * Return action name
	 *
	 * @return string
	 */
	public function getActionName() {
		return $this->actionName;
	}

	/**
	 * Return request format
	 *
	 * @return string
	 */
	public function getFormat() {
		return $this->format;
	}

	/**
	 * Instantiate configured service and run action, return service response
	 *
	 * return array|null
	 */
	public function handle() {
		$cn = $this->serviceName;
		$an = $this->actionName;
		require_once __DIR__ . "/../services/{$cn}.php";
		$service = new $cn();
		$out = $service->$an($this->id);
		$this->httpStatusCode = $service->getHttpStatusCode();
		$this->location = $service->getLocation();
		return $out;
	}

	/**
	 * Send out HTTP headers set by service
	 *
	 * @return Dispatcher
	 */
	public function sendHeaders() {
		$this->httpStatusCode and http_response_code($this->httpStatusCode);
		$this->location and header("Location: " . urlencode($this->location));
		return $this;
	}

}