<?php

/**
 * Helper class for common database operations using PDO interface
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class PDOHelper {

	protected $pdo;
	protected $lastQuery = "";
	protected $lastParams = [ ];

	/**
	 * Constrcutor
	 *
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function __construct(PDO $pdo = null) {
		$pdo
			? $this->pdo = $pdo
			: $this->pdo = DB::get();
	}

	/**
	 * Return last executed query
	 *
	 * @param bool $withParams
	 * @return string
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function getLastExecutedQuery($withParams = false) {
		if (!$withParams) {
			return $this->lastQuery;
		}
		$columns = [ ];
		$values = [ ];
		foreach ($this->lastParams as $column => $value) {
			$columns[] = ":{$column}";
			$values[] = is_numeric($value) ? $value : "'{$value}'";
		}
		return str_replace($columns, $values, $this->lastQuery);
	}

	/**
	 * Fetch and return single cell from database
	 *
	 * @param string $column
	 * @param string $sql
	 * @param array $params
	 * @return scalar
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function fetchCell($column, $sql, array $params = [ ]) {
		// prepare statement and bind parameters
		$stmt = $this->pdo->prepare($sql);
		foreach ($params as $param => $value) {
			strpos($sql, ":{$param}") and $stmt->bindValue($param, $value);
		}

		// make call and extract cell
		$this->lastQuery = $sql;
		$this->lastParams = $params;
		$stmt->execute();
		$out = $stmt->fetch();
		return $out[$column];
	}

	/**
	 * Fetch and return single row from database
	 *
	 * @param string $sql
	 * @param array $params
	 * @return scalar[]
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function fetchRow($sql, array $params = [ ]) {
		// prepare statement and bind parameters
		$stmt = $this->pdo->prepare($sql);
		foreach ($params as $param => $value) {
			strpos($sql, ":{$param}") and $stmt->bindValue($param, $value);
		}

		// make call and fetch fow
		$this->lastQuery = $sql;
		$this->lastParams = $params;
		$stmt->execute();
		return $stmt->fetch();
	}

	/**
	 * Fetch and return rows from database
	 *
	 * @param string $sql
	 * @param array $params
	 * @return scalar[][]
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function fetchAll($sql, array $params = [ ]) {
		// prepare statement and bind parameters
		$stmt = $this->pdo->prepare($sql);
		foreach ($params as $param => $value) {
			strpos($sql, ":{$param}") and $stmt->bindValue($param, $value);
		}

		// make call and fetch fow
		$this->lastQuery = $sql;
		$this->lastParams = $params;
		$stmt->execute();
		return $stmt->fetchAll();
	}

	/**
	 * Insert into database and return primary key or FALSE
	 *
	 * @param string $tableName
	 * @param array $params
	 * @return mixed|FALSE
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function insert($tableName, array $params) {
		// prepare placeholders
		$columns = array_keys($params);
		$placeholders = [ ];
		foreach ($params as $column => $value) {
			$cols[] = "`$column`";
			$placeholders[] = ":$column";
		}

		// prepare stamement
		$c = implode(", ", $cols);
		$p = implode(", ", $placeholders);
		$sql = "INSERT INTO $tableName ($c) VALUES ($p)";
		$pdo = $this->pdo;
		$stmt = $pdo->prepare($sql);

		// bind parameters
		foreach ($params as $column => $value) {
			$stmt->bindValue($column, $value);
		}

		// insert and return primary key (if exist) or true
		$this->lastQuery = $sql;
		$this->lastParams = $params;
		return $stmt->execute() ? $pdo->lastInsertId() : false;
	}

	/**
	 * Update database table and return affected rows count or FALSE
	 *
	 * @param string $tableName
	 * @param array $params
	 * @param string $where
	 * @param array $whereParams
	 * @return int|FALSE
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function update($tableName, array $params, $where, array $whereParams = [ ]) {
		// prepare updates
		$updates = [ ];
		foreach ($params as $column => $value) {
			$updates[] = "`$column` = :$column";
		}
		$updatesList = implode(", ", $updates);

		// prepare stamement
		$sql = "UPDATE $tableName SET $updatesList WHERE $where";
		$stmt = $this->pdo->prepare($sql);

		// bind parameters
		foreach ($params as $column => $value) {
			$stmt->bindValue($column, $value);
		}
		foreach ($whereParams as $column => $value) {
			$stmt->bindValue($column, $value);
		}

		// update and return affected rows count
		$this->lastQuery = $sql;
		$this->lastParams = array_merge($params, $whereParams);
		return $stmt->execute() ? $stmt->rowCount() : false;
	}

	/**
	 * Delete from database table and return affected rows count or FALSE
	 *
	 * @param string $tableName
	 * @param string $where
	 * @param array $whereParams
	 * @return int|FALSE
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function delete($tableName, $where, array $whereParams = [ ]) {
		// prepare stamement
		$sql = "DELETE FROM $tableName WHERE $where";
		$stmt = $this->pdo->prepare($sql);

		// bind parameters
		foreach ($whereParams as $column => $value) {
			$stmt->bindValue($column, $value);
		}

		// delete and return affected rows count
		$this->lastQuery = $sql;
		$this->lastParams = $whereParams;
		return $stmt->execute() ? $stmt->rowCount() : false;
	}

	/**
	 * Begin transaction
	 */
	public function begin() {
		$this->pdo->beginTransaction();
	}

	/**
	 * Commit transaction
	 */
	public function commit() {
		$this->pdo->commit();
	}

	/**
	 * Roll back transaction
	 */
	public function rollBack() {
		$this->pdo->rollBack();
	}

	/**
	 * Set engine property
	 *
	 * @param string $property
	 * @param string $value
	 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
	 */
	public function set($property, $value) {
		return $this->pdo->exec("SET $property = $value");
	}

}