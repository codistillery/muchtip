<?php

/**
 * Abstract presenter
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
abstract class Presenter {

	/**
	 * Output format
	 * @var string
	 */
	protected $format;

	/**
	 * Template file to use
	 * @var string
	 */
	protected $file;

	protected static $formatToMimeMap = [
		"json" => "application/json",
		"html" => "text/html",
	];

	/**
	 * Constructor
	 *
	 * @param string $format
	 * @param string $file
	 */
	public function __construct($format, $file) {
		$this->format = $format;
		$this->file = $file;
	}

	/**
	 * Send out HTTP headers
	 *
	 * @return Presenter
	 */
	public function sendHeaders() {
		$mime = isset(self::$formatToMimeMap[$this->format])
			? self::$formatToMimeMap[$this->format]
			: "text/html";
		header("Content-Type: $mime");
		header("Content-Disposition: inline");
		return $this;
	}

	/**
	 * Return formatted response
	 *
	 * @param array $response
	 * @return string
	 */
	abstract function format(array $response);

	/**
	 * Instantiate and return presenter according to request details
	 *
	 * @param string $format
	 * @param string $serviceName
	 * @param string $actionName
	 * @return Presenter
	 */
	public static function factory($format, $serviceName, $actionName) {
		if ("json" === $format) {
			return new PresenterJson("json", null);
		}
		$file = "{$serviceName}/{$actionName}.{$format}.twig";
		if (file_exists(TEMPLATES_DIR . $file)) {
			return new PresenterTwig($format, $file);
		}
		$file = "{$serviceName}/{$actionName}.{$format}.phtml";
		if (file_exists(TEMPLATES_DIR . $file)) {
			return new PresenterPhtml($format, $file);
		}
		throw new PresenterException("Template not found for action {$serviceName}::{$actionName}");
	}

}

class PresenterException extends Exception { }