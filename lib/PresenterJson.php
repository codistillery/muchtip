<?php

/**
 * Presenter formatting response as JSON string
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class PresenterJson extends Presenter {

	/**
	 * Return JSON formatted response
	 *
	 * @param array $response
	 * @return string
	 */
	public function format(array $response) {
		return json_encode($response, JSON_PRETTY_PRINT);
	}

}