<?php

/**
 * Presenter formating response with plain PHP code
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class PresenterPhtml extends Presenter {

	/**
	 * Return response formatted using Twig template
	 *
	 * @return string
	 */
	public function format(array $response) {
		ob_start();
		extract($response, EXTR_SKIP);
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

}