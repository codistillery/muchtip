<?php

/**
 * Presenter formating response as Twig template
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class PresenterTwig extends Presenter {

	/**
	 * Return response formatted using Twig template
	 *
	 * @return string
	 */
	public function format(array $response) {
		return TwigFactory::get()->render($this->file, $response);
	}

}