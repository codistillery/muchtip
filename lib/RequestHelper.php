<?php

/**
 * Request Helper
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
final class RequestHelper {

	/**
	 * Return GET variable
	 *
	 * @param string $var name
	 * @param mixed $default if variable not present, default null
	 * @return mixed
	 */
	public static function get($var, $default = null) {
		return isset($_GET[$var]) ? $_GET[$var] : $default;
	}

	/**
	 * Return POST variable
	 *
	 * @param string $var name
	 * @param mixed $default if variable not present, default null
	 * @return mixed
	 */
	public static function post($var, $default = null) {
		return isset($_POST[$var]) ? $_POST[$var] : $default;
	}

	/**
	 * Return cookie value
	 *
	 * @param string $var name
	 * @param mixed $default if cookie not present, default null
	 * @return mixed
	 */
	public static function cookie($var, $default = null) {
		return isset($_COOKIE[$var]) ? $_COOKIE[$var] : $default;
	}

	/**
	 * Return session variable
	 *
	 * @param string $var name
	 * @param mixed $default if cookie not present, default null
	 * @return mixed
	 */
	public static function session($var, $default = null) {
		return isset($_SESSION[$var]) ? $_SESSION[$var] : $default;
	}

	/**
	 * Return environment, GET, POST, cookie, or session variable (whichever is first, see EGPCS)
	 *
	 * @param string $var name
	 * @param mixed $default if variable not present, default null
	 * @return mixed
	 */
	public static function request($var, $default = null) {
		return isset($_REQUEST[$var]) ? $_REQUEST[$var] : $default;
	}

	/**
	 * Return path info
	 *
	 * @param mixed $default if path info not available, default null
	 * @return mixed
	 */
	public static function path($default = null) {
		return isset($_SERVER["PATH_INFO"]) ? $_SERVER["PATH_INFO"] : $default;
	}

	/**
	 * Return client IP address
	 *
	 * @return string
	 */
	public static function ip() {
		if (isset($_SERVER)) {
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
				return $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			if (isset($_SERVER["HTTP_CLIENT_IP"])) {
				return $_SERVER["HTTP_CLIENT_IP"];
			}
			return $_SERVER["REMOTE_ADDR"];
		}
		if (getenv("HTTP_X_FORWARDED_FOR")) {
			return getenv("HTTP_X_FORWARDED_FOR");
		}
		if (getenv("HTTP_CLIENT_IP")) {
			return getenv("HTTP_CLIENT_IP");
		}
		return getenv("REMOTE_ADDR");
	}

	/**
	 * Return HTTP method name
	 *
	 * @return string
	 */
	public static function method() {
		return isset($_SERVER["REQUEST_METHOD"]) ? $_SERVER["REQUEST_METHOD"] : "GET";
	}

	/**
	 * Return whether protocol is https://
	 *
	 * @return boolean
	 */
	public static function isHttps() {
		return isset($_SERVER["HTTPS"]) and "off" !== $_SERVER["HTTPS"];
	}

}