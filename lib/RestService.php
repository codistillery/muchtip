<?php

/**
 * Abstract RESTful service
 */
abstract class RestService {

	const OK = 200;
	const CREATED = 201;
	const ACCEPTED = 202;
	const NO_CONTENT = 204;
	const RESET_CONTENT = 205;

	const MOVED_PERMANENTLY = 301;
	const TEMPORARY_REDIRECT = 307;

	const BAD_REQUEST = 400;
	const FORBIDDEN = 403;
	const NOT_FOUND = 404;
	const METHOD_NOT_ALLOWED = 405;
	const NOT_ACCEPTABLE = 406;

	const INTERNAL_SERVER_ERROR = 500;

	protected $httpStatusCode = 200;
	protected $location;

	/**
	 * Action for GET request without "id" parameter
	 *
	 * @return array
	 */
	public function index() {
		return $this->respond(self::NO_CONTENT, false, "");
	}

	/**
	 * Action for GET request with "id" parameter
	 *
	 * @return array
	 */
	public function get($id) {
		return $this->respond(self::NO_CONTENT, false, "");
	}

	/**
	 * Action for PUT request
	 *
	 * @return array
	 */
	public function put() {
		return $this->respond(self::METHOD_NOT_ALLOWED, false, "");
	}

	/**
	 * Action for POST request
	 *
	 * @return array
	 */
	public function post($id) {
		return $this->respond(self::METHOD_NOT_ALLOWED, false, "");
	}

	/**
	 * Action for DELETE request
	 *
	 * @return array
	 */
	public function delete($id) {
		return $this->respond(self::METHOD_NOT_ALLOWED, false, "");
	}

	/**
	 * Format response in common format
	 *
	 * @param int $httpStatusCode
	 * @param boolean $ok
	 * @param string $message
	 * @param array $payload
	 * @return array
	 */
	protected function respond($httpStatusCode, $ok, $message, array $payload = [ ]) {
		$this->httpStatusCode = $httpStatusCode;
		return [
			"ok" => (boolean)$ok,
			"message" => (string)$message,
			"payload" => $payload,
		];
	}

	/**
	 * Set redirection
	 *
	 * @param int $httpStatusCode
	 * @param string $location
	 */
	protected function redirect($httpStatusCode, $location) {
		$this->httpStatusCode = $httpStatusCode;
		$this->location = $location;
	}

	/**
	 * Return HTTP status code
	 *
	 * @return int
	 */
	public function getHttpStatusCode() {
		return $this->httpStatusCode;
	}

	/**
	 * Return redirect location
	 *
	 * @return string
	 */
	public function getLocation() {
		return $this->location;
	}

}