<?php

/**
 * Service locator
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class ServiceLocator {

	/**
	 * Master switch
	 * @var boolean
	 */
	protected static $enabled = false;

	/**
	 * Bindings
	 * @var string[]
	 */
	protected static $bindings = [];

	/**
	 * Bind class as another class or instance
	 *
	 * @param string $className
	 * @param string|object $as
	 */
	public static function bind($className, $as) {
		if (!is_object($as) and !is_string($as) and !is_callable($as)) {
			throw new Exception("Only string, object, or callable can be used as binding");
		}
		$type = is_callable($as) ? "callable" : "class";
		isset(self::$bindings[$className][$type]) or self::$bindings[$className][$type] = [];
		self::$bindings[$className][$type] = $as;
		self::$enabled = true;
	}

	/**
	 * Unbind class
	 *
	 * @param string $className
	 */
	public static function unbind($className) {
		if (isset(self::$bindings[$className])) {
			unset(self::$bindings[$className]);
		}
	}

	/**
	 * Remove all bindings
	 */
	public static function expunge() {
		self::$bindings = [];
	}

	/**
	 * Instantiate class or bound service
	 *
	 * @param string $className
	 * @param array $params
	 * @return object
	 */
	public static function instantiate($className, array $params = []) {
		// use callable if defined
		if (true === self::$enabled and isset(self::$bindings[$className]["callable"])) {
			return call_user_func_array(self::$bindings[$className]["callable"], $params);
		}
		// fall back to class name
		$cn = (true === self::$enabled and isset(self::$bindings[$className]))
			? self::$bindings[$className]["class"]
			: $className;
		true === is_string($cn) or $cn = get_class($cn);
		return (new ReflectionClass($cn))->newInstanceArgs($params);
	}

	/**
	 * Call static method on bound service
	 *
	 * @param string $className
	 * @param string $method
	 * @param array $params
	 * @return mixed
	 */
	public static function callStatic($className, $method, array $params = []) {
		$cn = (true === self::$enabled and isset(self::$bindings[$className]) and isset(self::$bindings[$className]["class"]))
			? self::$bindings[$className]["class"]
			: $className;
		true === is_string($cn) or $cn = get_class($cn);
		return call_user_func_array([$cn, $method], $params);
	}

}