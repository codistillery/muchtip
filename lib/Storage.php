<?php

class Storage {

	protected $helper;

	protected static $instance;

	/**
	 * Constructor
	 */
	protected function __construct() {
		$this->helper = new PDOHelper();
	}

	/**
	 * Mock singleton instance
	 *
	 * @param Storage $instance
	 */
	public static function mockInstance(Storage $instance) {
		self::$instance = $instance;
	}

	/**
	 * Return singleton instance
	 *
	 * @return Storage
	 */
	public static function getInstance() {
		self::$instance or self::$instance = new Storage();
		return self::$instance;
	}

	/**
	 * Return database helper
	 *
	 * @return PDOHelper
	 */
	public function getHelper() {
		return $this->helper;
	}

	/**
	 * Retrieve row from database and push it into object
	 *
	 * @param object $object
	 * @param string $tableName
	 * @param string $column
	 * @param string $value
	 * @return object
	 */
	public function loadOne($object, $tableName, $column, $value) {
		$sql = "SELECT * FROM {$tableName} WHERE {$column} = :v";
		$row = (array)$this->helper->fetchRow($sql, ["v" => $value]);
		foreach ($row as $property => $value) {
			$property = lcfirst(str_replace(" ", "", ucwords(str_replace("_", " ", $property))));
			$object->$property = $value;
		}
		return $object;
	}

	/**
	 * Retrieve collection of rows from database and push it into objects created from prototype
	 *
	 * @param object $prototype
	 * @param string $tableName
	 * @param string $column
	 * @param string $value
	 * @return object[]
	 */
	public function loadAll($prototype, $tableName, $column, $value) {
		$all = [];
		$sql = "SELECT * FROM {$tableName} WHERE {$column} = :v";
		$rows = $this->helper->fetchAll($sql, ["v" => $value]);
		foreach ($rows as $row) {
			$object = clone $prototype;
			foreach ($row as $property => $value) {
				$property = lcfirst(str_replace(" ", "", ucwords(str_replace("_", " ", $property))));
				$object->$property = $value;
			}
			$all[] = $object;
		}
		return $all;
	}

	public function loadInto($prototype, $sql, $params) {
		$all = [];
		$rows = $this->helper->fetchAll($sql, $params);
		foreach ($rows as $row) {
			$object = clone $prototype;
			foreach ($row as $property => $value) {
				$property = lcfirst(str_replace(" ", "", ucwords(str_replace("_", " ", $property))));
				$object->$property = $value;
			}
			$all[] = $object;
		}
		return $all;
	}

	/**
	 * Store object into database row
	 *
	 * @param object $o
	 * @param string $tableName
	 * @param string $primaryKey column
	 * @return boolean
	 */
	public function save($o, $tableName, $primaryKey) {
		$params = [];
		$callback = static function ($matches) {
			return "_" . strtolower($matches[0]);
		};
		foreach (get_object_vars($o) as $property => $value) {
			$property = preg_replace_callback("/[A-Z]/", $callback, $property);
			$params[$property] = $value;
		}
		// on update
		if (isset($params[$primaryKey])) {
			return (boolean)$this->helper->update($tableName, $params, "{$primaryKey} = :pkv", ["pkv" => $params[$primaryKey]]);
		}
		// on insert
		unset($params[$primaryKey]);
		if ($id = $this->helper->insert($tableName, $params)) {
			$o->$primaryKey = $id;
		}
		return (boolean)$id;
	}

}