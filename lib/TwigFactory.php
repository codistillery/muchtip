<?php

/**
 * Factory for pre-configured instance of Twig templating engine
 *
 * Lazily instantiates Twig.
 * Maintains its cached instance for future reuse.
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
final class TwigFactory {

	/**
	 * @var Twig_Environment
	 */
	protected static $twig;

	/**
	 * Return singleton instance of Twig
	 *
	 * @return Twig_Environment
	 */
	public static function get() {
		if (null === self::$twig) {
			$loader = new Twig_Loader_Filesystem(TEMPLATES_DIR);
			self::$twig = new Twig_Environment($loader, array(
    			"cache" => TWIG_CACHE_DIR,
				"auto_reload" => true,
			));
		}
		return self::$twig;
	}
}