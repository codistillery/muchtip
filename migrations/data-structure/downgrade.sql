DROP TABLE IF EXISTS payouts;
DROP TABLE IF EXISTS tips;
DROP VIEW IF EXISTS source_addresses_view;
DROP TABLE IF EXISTS source_addresses;
DROP VIEW IF EXISTS source_urls_view;
DROP TABLE IF EXISTS source_urls;
DROP TABLE IF EXISTS sources;
DROP TABLE IF EXISTS addresses;
