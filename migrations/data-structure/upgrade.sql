-- content sources
DROP TABLE IF EXISTS sources CASCADE;
CREATE TABLE sources (
	id				INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	created_on		TIMESTAMP NOT NULL,
	domain			CHAR(100) NOT NULL COMMENT "Source domain name",
	url_hash		CHAR(40) NOT NULL COMMENT "MD5 hash of source URL"
) ENGINE MyISAM DEFAULT CHARSET ascii ROW_FORMAT FIXED;

CREATE INDEX sources_domain_idx ON sources (domain);
CREATE UNIQUE INDEX sources_url_hash_unq ON sources (url_hash);

-- full urls for content sources
DROP TABLE IF EXISTS urls CASCADE;
CREATE TABLE urls (
	source_id		INT UNSIGNED NOT NULL REFERENCES sources (id),
	url				TEXT
) ENGINE MyISAM DEFAULT CHARSET utf8;

CREATE UNIQUE INDEX urls_source_id_unq ON urls (source_id);

-- tip jars
DROP TABLE IF EXISTS jars CASCADE;
CREATE TABLE jars (
	id				INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	source_id		INT UNSIGNED NOT NULL REFERENCES sources (id),
	authors_address	CHAR(40) NOT NULL COMMENT "Author's address",
	jar_address		CHAR(40) NOT NULL COMMENT "Tip jar address",
	currency		CHAR(4) NOT NULL,
	created_on		TIMESTAMP NOT NULL,
	expires_on		TIMESTAMP NOT NULL
) ENGINE MyISAM DEFAULT CHARSET ascii ROW_FORMAT FIXED;

CREATE INDEX jars_source_id_idx ON jars (source_id);
CREATE UNIQUE INDEX jars_authors_address_currency_unq ON jars (authors_address, currency);
CREATE UNIQUE INDEX jars_jar_address_currency_unq ON jars (jar_address, currency);

-- tips received
DROP TABLE IF EXISTS tips CASCADE;
CREATE TABLE tips (
	id				INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	received_on		TIMESTAMP NOT NULL,
	jar_id			INT UNSIGNED NOT NULL REFERENCES jars (id),
	currency		CHAR(4) NOT NULL,
	amount			INT UNSIGNED NOT NULL,
	donors_address	CHAR(40) NOT NULL COMMENT "Donor's address",
	txid			CHAR(40) NOT NULL
) ENGINE MyISAM DEFAULT CHARSET ascii ROW_FORMAT FIXED;

CREATE INDEX tips_jar_id_currency_idx ON tips (jar_id, currency);
CREATE UNIQUE INDEX tips_txid_unq ON tips (txid);

-- coins paid out
DROP TABLE IF EXISTS payouts CASCADE;
CREATE TABLE payouts (
	id				INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	tip_id			INT UNSIGNED NOT NULL REFERENCES tips (id),
	type			ENUM("A", "F", "D", "L") NOT NULL,
	payout_addr		CHAR(40) NOT NULL COMMENT "Target address to which payout is made",
	amount			INT UNSIGNED NOT NULL,
	currency		CHAR(4) NOT NULL,
	created_on		TIMESTAMP NOT NULL,
	cleared_on		TIMESTAMP NULL,
	txid			CHAR(40) NULL COMMENT "Payout transaction id or NULL if not yet processed"
) ENGINE MyISAM DEFAULT CHARSET ascii ROW_FORMAT FIXED;

CREATE UNIQUE INDEX payouts_tip_id_unq ON payouts (tip_id);
CREATE INDEX payouts_tip_id_idx ON payouts (tip_id);
CREATE INDEX payouts_currency_idx ON payouts (currency);
CREATE INDEX payouts_cleared_on_idx ON payouts (cleared_on);
CREATE INDEX payouts_txid_idx ON payouts (txid);
