<?php

/**
 * Tip jar
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Jar {

	const TABLE_NAME = "jars";

	public $id;
	public $sourceId;
	public $authorsAddress;
	public $jarAddress;
	public $currency;
	public $createdOn;
	public $expiresOn;

	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Fetch jar from database by primary key
	 *
	 * @param int $id
	 * @return Jar
	 */
	public static function getById($id) {
		return Storage::getInstance()->loadOne(new Jar(), self::TABLE_NAME, "id", $id);
	}

	/**
	 * Fetch jar from database by tipping address
	 *
	 * @param string $currency
	 * @param string $address
	 * @return Jar
	 */
	public static function getByTippingAddress($currency, $address) {
		$tableName = self::TABLE_NAME;
		$sql = "
			SELECT * FROM $tableName
			WHERE currency = :currency AND jar_address = :address
		";
		$jar = new Jar();
		$jars = Storage::getInstance()->loadInto($jar, $sql, ["currency" => $currency, "address" => $address]);
		if (empty($jars)) {
			return $jar;
		}
		return $jars[0];
	}

	/**
	 * Fetch all jars from database
	 *
	 * @return Jar[]
	 */
	public static function getAll() {
		return Storage::getInstance()->loadInto(new Jar(), "SELECT * FROM " . self::TABLE_NAME, []);
	}

	/**
	 * Return whether jar exists
	 *
	 * @return boolean
	 */
	public function exists() {
		return null !== $this->id and null !== $this->jarAddress;
	}

	/**
	 * Store jar in database
	 *
	 * @return boolean
	 */
	public function save() {
		$this->createdOn or $this->createdOn = DATETIME;
		$storage = ServiceLocator::callStatic("Storage", "getInstance");
		return $storage->save($this, self::TABLE_NAME, "id");
	}

}