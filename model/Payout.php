<?php

/**
 * Payout
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Payout {

	public $id;
	public $tipId;
	public $type;
	public $payoutAddress;
	public $amount;
	public $currency;
	public $createdOn;
	public $clearedOn;
	public $txid;

	const TABLE_NAME = "payouts";

	const TYPE_AUTHOR = "A";
	const TYPE_FEE = "F";
	const TYPE_DIVIDEND = "D";
	const TYPE_LEFTOVERS = "L";

	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Fetch payout from database by primary key
	 *
	 * @param int $id
	 * @return Payout
	 */
	public static function getById($id) {
		return Storage::getInstance()->loadOne(new Payout(), self::TABLE_NAME, "id", $id);
	}

	/**
	 * Fetch unprocessed payments from database
	 *
	 * @param string $currency
	 * @return Payout[]
	 */
	public static function getUnprocessed($currency) {
		$tableName = self::TABLE_NAME;
		$sql = "
			SELECT * FROM $tableName
			WHERE cleared_on IS NULL AND currency = :currency
		";
		return Storage::getInstance()->loadInto(new Payout(), $sql, ["currency" => $currency]);
	}

	/**
	 * Return whether payout exists
	 *
	 * @return boolean
	 */
	public function exists() {
		return null !== $this->id;
	}

	/**
	 * Store tip in database
	 *
	 * @return boolean
	 */
	public function save() {
		$this->createdOn or $this->createdOn = DATETIME;
		if ($this->txid and !$this->clearedOn) {
			$this->clearedOn = date("Y-m-d H:i:s");
		}
		$storage = ServiceLocator::callStatic("Storage", "getInstance");
		return $storage->save($this, self::TABLE_NAME, "id");
	}

}