<?php

/**
 * Transaction processor
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Processor {

	protected $currency;

	/**
	 * Constructor
	 *
	 * @param string $currency
	 */
	public function __construct($currency) {
		$this->currency = $currency;
	}

	/**
	 * Process incoming transaction
	 *
	 * @param string $txid
	 * @return Tip[]
	 */
	public function receive($txid) {
		// fetch transaction details
		$client = ServiceLocator::callStatic("Blockchain", "getClient", [$this->currency]);
		$txn = $client->gettransaction($txid);
		$tips = [];

		// check if valid transaction
		if (empty($txn)) {
			throw new ProcessorException("Txn $txid not found");
		}

		// loop through unspent outputs
		foreach ($txn["details"] as $n => $out) {

			// filter out other than received
			if ("receive" !== $out["category"]) {
				continue;
			}

			// get output details
			$amount = $out["amount"];
			$targetAddress = $out["address"];
			// TODO log "Txn $txid found: $amount $currency -> '$account' ($address), n={$n}"

			// fetch tipping jar
			$jar = ServiceLocator::callStatic("Jar", "getByTippingAddress", [$this->currency, $targetAddress]);
			if (!$jar->exists()) {
				// TODO log "Tipping address {$this->currency} {$targetAddress} not associated with any jar"
				// TODO add test
				continue;
			}

			// find donor's address
			$donorsAddress = $client->findFromAddress($txid);
			if (!$donorsAddress) {
				// TODO log "Donor's address not found"
				// TODO add test
				continue;
			}

			// record tip
			try {
				$tip = ServiceLocator::callStatic("Tip", "receive", [$jar, $amount, $donorsAddress, $txid]);
			} catch (Exception $e) {
				// TODO log "Error receiving tip"
				// TODO add test
				continue;
			}
			$tips[] = $tip;
			// TODO log "Tip #{$tip->id} created"
		}
		return $tips;
	}

	/**
	 * Send payouts
	 *
	 * @return Payout[]
	 */
	public function send() {
		$processed = [];
		$sql = "
			SELECT
				p.id AS id,
				j.jar_address AS from,
				p.payout_address AS to,
				p.amount
			FROM payouts AS p
			INNER JOIN tips AS t ON t.id = p.tip_id
			INNER JOIN jars AS j ON j.id = t.jar_id
			WHERE p.txid IS NULL AND p.currency = :currecny
			ORDER BY from, amount
		";
		$helper = ServiceLocator::instantiate("PDOHelper");
		$out = $helper->fetchAll($sql, ["currency" => $this->currency]);

		$many = [];
		foreach ($out as $row) {
			$id = $row["id"];
			$from = $row["from"];
			$to = $row["to"];
			$amount = $row["amount"];

			isset($many[$from]) or $many[$from] = ["ids" => [], "many" => []];
			isset($many[$from]["many"][$to]) or $many[$from]["many"][$to] = 0;
			$many[$from]["ids"][] = $id;
			$many[$from]["many"][$to] += $amount;

		}

		// process each batch
		$client = ServiceLocator::callStatic("Blockchain", "getClient", [$this->currency]);
		foreach ($many as $from => $batch) {
			$txid = $client->sendmany($from, $batch["many"], 1);
			$params = [
				"txid" => $txid,
				"cleared_on" => DATETIME,
			];
			$ids = implode(", ", $batch["ids"]);
			$ok = $helper->update(Payout::TABLE_NAME, $params, "id IN (:list)", ["list" => $ids]);
			$ok and $processed[] = $txid;
		}

		return $processed;
	}

}

class ProcessorException extends Exception { }