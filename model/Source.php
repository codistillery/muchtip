<?php

/**
 * Source
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Source {

	const TABLE_NAME = "sources";

	public $id;
	public $createdOn;
	public $domain;
	public $urlHash;

	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Fetch source from database by either primary key, URL or URL hash
	 *
	 * @param int $id
	 * @return Source
	 */
	public static function get($id) {
		if (is_numeric($id)) {
			return Source::getById($id);
		}
		$source = Source::getByUrlHash(md5($id));
		$source->exists() or $source = Source::getByUrl($id);
		return $source;
	}

	/**
	 * Fetch source from database by primary key
	 *
	 * @param int $id
	 * @return Source
	 */
	public static function getById($id) {
		return Storage::getInstance()->loadOne(new Source(), self::TABLE_NAME, "id", $id);
	}

	/**
	 * Fetch source from database by URL
	 *
	 * @param string $url
	 * @return Source
	 */
	public static function getByUrl($url) {
		return Storage::getInstance()->loadOne(new Source(), self::TABLE_NAME, "url_hash", md5($url));
	}

	/**
	 * Fetch source from database by URL hash
	 *
	 * @param string $hash
	 * @return Source
	 */
	public static function getByUrlHash($hash) {
		return Storage::getInstance()->loadOne(new Source(), self::TABLE_NAME, "url_hash", $hash);
	}

	/**
	 * Fetch all sources from database
	 *
	 * @return Source[]
	 */
	public static function getAll() {
		return Storage::getInstance()->loadInto(new Source(), "SELECT * FROM " . self::TABLE_NAME, []);
	}

	/**
	 * Create source with given URL address
	 *
	 * @param string $url
	 * @return Source
	 */
	public static function create($url) {
		$domain = str_replace(["http://", "https://"], "", $url);
		$domain = preg_replace("/\/.*/", "", $domain);
		$domain = preg_replace("/.*@/", "", $domain);

		// store source into database
		$source = new Source();
		$source->domain = $domain;
		$source->urlHash = md5($url);
		$source->save();

		// store url into separate database table
		$h = ServiceLocator::instantiate("PDOHelper");
		$h->insert("urls", ["source_id" => $source->id, "url" => $url]);

		return $source;
	}

	/**
	 * Return whether source exists
	 *
	 * @return boolean
	 */
	public function exists() {
		return null !== $this->id and null !== $this->urlHash;
	}

	/**
	 * Store source in database
	 *
	 * @return boolean
	 */
	protected function save() {
		if ($this->exists()) {
			throw new SourceException("Source can only be saved once");
		}
		$this->createdOn = DATETIME;
		$storage = ServiceLocator::callStatic("Storage", "getInstance");
		return $storage->save($this, self::TABLE_NAME, "id");
	}

	/**
	 * Generate tip jar
	 *
	 * @param string $currency
	 * @param string $authorsAddress
	 * @param int|null $timeout in seconds, default see JAR_TIMEOUT
	 * @return string
	 */
	public function generateJar($currency, $authorsAddress, $timeout = null) {
		if (!$this->exists()) {
			throw new SourceException("Cannot obtain tip jar for non-existent source");
		}

		$account = "source_{$this->id}";
		$client = ServiceLocator::callStatic("Blockchain", "getClient", [$currency]);
		$jarAddress = $client->getnewaddress($account);

		$jar = new Jar();
		$jar->sourceId = $this->id;
		$jar->currency = $currency;
		$jar->authorsAddress = $authorsAddress;
		$jar->jarAddress = $jarAddress;
		$jar->createdOn = DATETIME;
		$jar->expiresOn = date("Y-m-d H:i:s", NOW + $timeout);
		$jar->save();

		return $jar;
	}

}