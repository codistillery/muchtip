<?php

/**
 * Tip
 *
 * @author Michał Rudnicki <michal.rudnicki@epsi.pl>
 */
class Tip {

	public $id;
	public $receivedOn;
	public $jarId;
	public $currency;
	public $amount;
	public $donorsAddress;
	public $txid;

	const TABLE_NAME = "tips";

	/**
	 * Constructor
	 */
	public function __construct() {

	}

	/**
	 * Fetch tip from database by primary key
	 *
	 * @param int $id
	 * @return Tip
	 */
	public static function getById($id) {
		return Storage::getInstance()->loadOne(new Tip(), self::TABLE_NAME, "id", $id);
	}

	/**
	 * Fetch tips from database by transaction id
	 *
	 * @param string $txid
	 * @return Tip
	 */
	public static function getByTxid($txid) {
		return Storage::getInstance()->loadAll(new Tip(), self::VIEW_NAME, "txid", $txid);
	}

	/**
	 * Receive and return tip object
	 *
	 * @param Jar $jar to which tip was put in
	 * @param int $amount in satoshis
	 * @param string $donosrAddress
	 * @param string $txid
	 */
	public static function receive(Jar $jar, $amount, $donorsAddress, $txid) {
		$tip = new Tip();
		$tip->receivedOn = DATETIME;
		$tip->jarId = $jar->id;
		$tip->currency = $jar->currency;
		$tip->amount = (int)$amount;
		$tip->donorsAddress = $donorsAddress;
		$tip->txid = $txid;
		if (!$tip->save()) {
			throw new TipException("Could not save tip in database, check txid $txid");
		}

		// get past tips
		$tipsTableName = self::TABLE_NAME;
		$jarsTableName = Jar::TABLE_NAME;
		$sourcesTableName = Source::TABLE_NAME;
		$sql = "
			SELECT t.* FROM $tipsTableName AS t
			INNER JOIN $jarsTableName AS j ON j.id = t.jar_id
			INNER JOIN $sourcesTableName AS s ON s.id = j.source_id
			WHERE s.id = :sourceId AND t.id <> :tipId
		";
		$storage = ServiceLocator::callStatic("Storage", "getInstance");
		$pastTips = $storage->loadInto(new Tip(), $sql, ["sourceId" => $jar->sourceId, "tipId" => $tip->id]);
		$n = count($pastTips);

		// calculate fee
		$fee = (int)(FEE_SHARE * $tip->amount);
		$left = (int)($tip->amount - $fee);

		// calculate author's share
		$authorsShare = (0 === $n)
			? $left // 100% if no previous tips
			: (int)(AUTHORS_SHARE * $left);
		$left -= $authorsShare;

		// calculate tippers' share value
		$total = 0;
		foreach ($pastTips as $pastTip) {
			$total += $pastTip->amount;
		}
		$unit = (0 === $total) ? 0 : ($left / $total); // single share value, float

		// calculate tippers' new shares proportionally
		$shares = [];
		foreach ($pastTips as $pastTip) {
			isset($shares[$pastTip->donorsAddress]) or $shares[$pastTip->donorsAddress] = 0;
			$amount = (int)($unit * $pastTip->amount);
			$shares[$pastTip->donorsAddress] += $amount;
			$left -= $amount;
		}

		// get fee address
		$currency = $jar->currency;
		$config = Config::getInstance();
		$feeAddress = (string)$config->fees->$currency;
		$leftoversAddress = (string)$config->leftovers->$currency;

		// prepare tip prototype
		$prototype = new Payout();
		$prototype->tipId = $tip->id;
		$prototype->currency = $tip->currency;

		// save fee
		if ($feeAddress) {
			$p = clone $prototype;
			$p->type = Payout::TYPE_FEE;
			$p->payoutAddress = $feeAddress;
			$p->amount = $fee;
			$p->save();
		}

		// save author's share
		$p = clone $prototype;
		$p->type = Payout::TYPE_AUTHOR;
		$p->payoutAddress = $jar->authorsAddress;
		$p->amount = $authorsShare;
		$p->save();

		// save tippers' new shares
		foreach ($shares as $donorsAddress => $amount) {
			if (0 === $amount) {
				continue;
			}
			$p = clone $prototype;
			$p->type = Payout::TYPE_DIVIDEND;
			$p->payoutAddress = $donorsAddress;
			$p->amount = $amount;
			$p->save();
		}

		// save leftovers
		if ($leftoversAddress and $left > 0) {
			$p = clone $prototype;
			$p->type = Payout::TYPE_LEFTOVERS;
			$p->payoutAddress = $leftoversAddress;
			$p->amount = $left;
			$p->save();
		}

		return $tip;
	}


	/**
	 * Return whether tip exists
	 *
	 * @return boolean
	 */
	public function exists() {
		return null !== $this->id;
	}

	/**
	 * Store tip in database
	 *
	 * @return boolean
	 */
	protected function save() {
		if ($this->exists()) {
			throw new Exception("Tips are immutable");
		}
		$this->receivedOn or $this->receivedOn = DATETIME;
		$storage = ServiceLocator::callStatic("Storage", "getInstance");
		return $storage->save($this, self::TABLE_NAME, "id");
	}

}

class TipException extends Exception { }