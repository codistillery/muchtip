<?php

ini_set("display_errors", "on");
error_reporting(E_ALL);

require __DIR__ . "/../bootstrap.php";

// prepare dispatcher
$dispatcher = new Dispatcher();
$dispatcher->configureFromRequest();

// dispatch request to service action
try {
	$response = $dispatcher->handle();
} catch (Exception $e) {
	$_GET["http"] = 500;
	$_GET["ok"] = false;
	$_GET["message"] = $e->getMessage() . "\n\n" . $e->getTraceAsString();
	$dispatcher->configure("DiagnosticService", "index");
	$response = $dispatcher->handle();
}

// render output
try {
	$format = $dispatcher->getFormat();
	$presenter = Presenter::factory($format, $dispatcher->getServiceName(), $dispatcher->getActionName());
} catch (Exception $e) {
	$presenter = Presenter::factory("html", $dispatcher->getServiceName(), $dispatcher->getActionName());
}
$out = $presenter->format($response);

// send headers and response body
$dispatcher->sendHeaders();
$presenter->sendHeaders();
print($out);
