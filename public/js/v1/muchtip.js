'use strict';
var Muchtip = function ($, url, addresses) {

	var sourceId;
	var jars = [];

	var endpoint = "http://api.muchtip.com";

	var that = this;

	var api = {
		getSourceId: function (url, successCallback, errorCallback) {
			$.ajax({
				url: endpoint + "/source.json?id=" + encodeURIComponent(url),
				type: "GET",
				success: successCallback,
				error: errorCallback
			});
		},
		registerSource: function (url, successCallback, errorCallback) {
			$.ajax({
				url: endpoint + "/source.json?id=" + encodeURIComponent(url),
				type: "POST",
				success: successCallback,
				error: errorCallback
			});
		},
		generateJar: function (sourceId, currency, address, successCallback, errorCallback) {
			$.put(endpoint + "/sourcejar/" + sourceId + ".json", {currency: currency, address: address}, callback);
		}
	};

	this.open = function () {
		// TODO: show widget
	};

	this.close = function () {
		// TODO: hide widget
	};

	this.selectCurrency = function (currency) {
		if (undefined === jars[currency]) {
			var callback = function (data) {
				if (undefined !== data.ok && true === data.ok) {
					jars[currency] = data.payload.address;
					selectCurrency(currency); // recurse
				} else {
					// TODO: handle 400
				}
			};
			api.generateJar(sourceId, currency, addresses[currency], callback);
		} else {
			// TODO: switch QR code
		}
	};

	(function () {
		var successCallback = function (data) {
			if (undefined !== data.ok && true === data.ok) {
				sourceId = data.payload.id;
				that.open();
			}
		};
		var errorCallback = function (xhr, textStatus, errorThrown) {
			api.registerSource(url, successCallback, function () {});
		}
		api.getSourceId(url, successCallback, errorCallback);
	})();

};