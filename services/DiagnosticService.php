<?php

/**
 * Diagnostic service
 *
 * @author Michał Rudnicki <michal.rudnicki@telefonica.com>
 */
class DiagnosticService extends RestService {

	public function index() {
		$http = (int)RequestHelper::get("http");
		$ok = (boolean)RequestHelper::get("ok");
		$message = (string)RequestHelper::get("message");
		return $this->respond($http, $ok, $message, [ "time" => time() ]);
	}

}