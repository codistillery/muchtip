<?php

final class HomeService extends RestService {

	public function index() {
		return $this->respond(self::OK, true, "", []);
	}

}