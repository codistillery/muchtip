<?php

final class JarService extends RestService {

	/**
	 * List all jars
	 */
	public function index() {
		$out = [];
		$jars = ServiceLocator::callStatic("Jar", "getAll");
		foreach ($jars as $jar) {
			$out["{$jar->currency} {$jar->jarAddress}"] = BASE_URL . "jar/{$jar->id}";
		}
		return $this->respond(self::OK, true, "", $out);
	}

	/**
	 * Get single jar
	 *
	 * @param string $id
	 */
	public function get($id) {
		$jar = ServiceLocator::callStatic("Jar", "getById", [$id]);
		if (!$jar->exists()) {
			return $this->respond(self::NOT_FOUND, false, "Jar not found", []);
		}

		$details = [
			"id" => $jar->id,
			"source" => BASE_URL . "source/{$jar->sourceId}",
			"currency" => $jar->currency,
			"address" => $jar->jarAddress,
			"owner" => $jar->authorsAddress,
			"createdOn" => $jar->createdOn,
		];
		return $this->respond(self::OK, true, "", $details);
	}

}