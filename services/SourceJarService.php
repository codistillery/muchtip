<?php

final class SourceJarService extends RestService {

	/**
	 * Generate jar for given source
	 *
	 * @param string $id
	 */
	public function post($id) {
		$currency = RequestHelper::post("currency");
		$address = RequestHelper::post("address");

		// validate parameters
		if (!$currency) {
			return $this->respond(self::BAD_REQUEST, false, "Missing currency", []);
		}
		if (!$address) {
			return $this->respond(self::BAD_REQUEST, false, "Missing address", []);
		}

		// look up source
		$source = ServiceLocator::callStatic("Source", "get", [$id]);
		if (!$source->exists()) {
			return $this->respond(self::NOT_FOUND, false, "Source not found", []);
		}

		// generate jar
		$jar = $source->generateJar($currency, $address);

		// return jar details
		$details = [
			"id" => $jar->id,
			"jar" => BASE_URL . "jar/{$jar->id}",
			"currency" => $jar->currency,
			"address" => $jar->jarAddress,
		];
		return $this->respond(self::CREATED, true, "", $details);
	}

}