<?php

final class SourceService extends RestService {

	/**
	 * List sources index
	 */
	public function index() {
		$out = [];
		$sources = ServiceLocator::callStatic("Source", "getAll");
		foreach ($sources as $source) {
			$out[$source->urlHash] = BASE_URL . "source/{$source->id}";
		}
		return $this->respond(self::OK, true, "", $out);
	}

	/**
	 * Get signle source
	 *
	 * @param string $id
	 */
	public function get($id) {
		$source = ServiceLocator::callStatic("Source", "get", [$id]);
		if (!$source->exists()) {
			return $this->respond(self::NOT_FOUND, false, "Source not found", []);
		}

		$details = [
			"id" => $source->id,
			"hash" => $source->urlHash,
		];
		return $this->respond(self::OK, true, "", $details);
	}

	/**
	 * Create or retrieve single source
	 *
	 * @param string $id
	 */
	public function post($id) {
		$source = ServiceLocator::callStatic("Source", "get", [$id]);
		$existed = $source->exists();
		$existed or $source = ServiceLocator::callStatic("Source", "create", [$id]);

		$details = [
			"id" => $source->id,
			"hash" => $source->urlHash,
		];
		return $this->respond($existed ? self::OK : self::CREATED, true, "", $details);
	}

}