<?php

require_once __DIR__ . "/../services/JarService.php";

final class JarServiceTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function index() {
		$jar1 = new Jar();
		$jar1->id = 1;
		$jar1->sourceId = 1;
		$jar1->currency = "btc";
		$jar1->jarAddress = "17nMsWUzYiP3xSJTfJ8Vz9u6qZaNXdVdMw";
		$jar1->authorsAddress = "1KuHza8AKBVgLb6F6u1t17UcckmChE6gzQ";
		$jar2 = new Jar();
		$jar2->id = 1;
		$jar2->sourceId = 1;
		$jar2->currency = "doge";
		$jar2->jarAddress = "DFU3isaR5iYDhCc3UEGzfbct5gNs7SsLvU";
		$jar2->authorsAddress = "DG74RGp8n4EvGP4RHddnbT72KaexQzttmt";

		// mock jar
		$jarMock = $this->getMock("Jar");
		$jarMock->staticExpects($this->once())
			->method("getAll")
			->will($this->returnValue([$jar1, $jar2]));
		ServiceLocator::bind("Jar", $jarMock);

		// test service
		$service = new JarService();
		$out = $service->index();
		$this->assertEquals(200, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$expected = [
			"{$jar1->currency} {$jar1->jarAddress}" => BASE_URL . "jar/{$jar1->id}",
			"{$jar2->currency} {$jar2->jarAddress}" => BASE_URL . "jar/{$jar2->id}",
		];
		$this->assertEquals($expected, $out["payload"]);
	}

	/**
	 * @test
	 */
	public function get_200() {
		$jar = new Jar();
		$jar->id = 1;
		$jar->sourceId = 1;
		$jar->currency = "btc";
		$jar->jarAddress = "17nMsWUzYiP3xSJTfJ8Vz9u6qZaNXdVdMw";
		$jar->authorsAddress = "1KuHza8AKBVgLb6F6u1t17UcckmChE6gzQ";
		$jar->createdOn = DATETIME;
		$jar->expiresOn = DATETIME;

		// mock jar
		$jarMock = $this->getMock("Jar");
		$jarMock->staticExpects($this->once())
			->method("getById")
			->will($this->returnValue($jar));
		ServiceLocator::bind("Jar", $jarMock);

		// test service
		$service = new JarService();
		$out = $service->get(1);
		$this->assertEquals(200, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$this->assertEquals($jar->id, $out["payload"]["id"]);
		$this->assertEquals(BASE_URL . "source/{$jar->sourceId}", $out["payload"]["source"]);
		$this->assertEquals($jar->currency, $out["payload"]["currency"]);
		$this->assertEquals($jar->jarAddress, $out["payload"]["address"]);
		$this->assertEquals($jar->authorsAddress, $out["payload"]["owner"]);
		$this->assertEquals(DATETIME, $jar->createdOn);
	}

}