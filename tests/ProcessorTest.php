<?php

final class ProcessorTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function receive_ok() {
		$txid = "tx12345";
		$amount = 6000;
		$currency = "btc";
		$donorsAddress = "1HLCd7RVc9Mm98xGJ3ev48znW9d1VzQ7Du";

		// mock jar
		$jar = new Jar();
		$jar->id = 555;
		$jar->sourceId = 1;
		$jar->authorsAddress = "17nMsWUzYiP3xSJTfJ8Vz9u6qZaNXdVdMw";
		$jar->jarAddress = "1KuHza8AKBVgLb6F6u1t17UcckmChE6gzQ";
		$jar->currency = $currency;

		// mock transaction
		$txn = [
			"details" => [
				1 => [
					"category" => "receive",
					"amount" => $amount,
					"address" => $jar->jarAddress,
				],
			],
		];

		// mock blockchain client
		$blockchainMock = $this->getMockBuilder("Blockchain")
			->disableOriginalConstructor()
			->getMock();
		$blockchainMock->staticExpects($this->once())
			->method("getClient")
			->will($this->returnValue($blockchainMock));
		// get transaction
		$blockchainMock->expects($this->at(0))
			->method("__call")
			->with($this->equalTo("gettransaction"), [$txid])
			->will($this->returnValue($txn));
		// look up donor's address
		$blockchainMock->expects($this->at(1))
			->method("findFromAddress")
			->with($this->equalTo($txid))
			->will($this->returnValue($donorsAddress));
		ServiceLocator::bind("Blockchain", $blockchainMock);

		// mock tip jar
		$jarMock = $this->getMock("Jar");
		$jarMock->staticExpects($this->once())
			->method("getByTippingAddress")
			->with($this->equalTo($currency), $this->equalTo($jar->jarAddress))
			->will($this->returnValue($jar));
		ServiceLocator::bind("Jar", $jarMock);

		// mock tip
		$tipMock = $this->getMock("Tip");
		$tipMock->staticExpects($this->once())
			->method("receive")
			->with($this->equalTo($jar), $this->equalTo($amount), $this->equalTo($donorsAddress), $this->equalTo($txid))
			->will($this->returnValue(new Tip()));
		ServiceLocator::bind("Tip", $tipMock);

		// validate
		$processor = new Processor($currency);
		$tips = $processor->receive($txid);
		$this->assertEquals(1, count($tips));
	}

	/**
	 * @test
	 */
	public function receive_txidNotFound() {
		$this->markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function receive_tippingAddressNotAssociatedWithJar() {
		$this->markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function receive_donorsAddressNotFound() {
		$this->markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function receive_failedToReceiveTip() {
		$this->markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function send_ok() {
		$currency = "btc";

		// mock rows returned
		$rows = [
			["id" => 1, "from" => "FROM1", "to" => "TO1", "amount" => "100"],
			["id" => 2, "from" => "FROM2", "to" => "TO1", "amount" => "200"],
			["id" => 3, "from" => "FROM2", "to" => "TO2", "amount" => "300"],
			["id" => 4, "from" => "FROM3", "to" => "TO2", "amount" => "400"],
		];

		// mock blockchain client
		$blockchainMock = $this->getMockBuilder("Blockchain")
			->disableOriginalConstructor()
			->getMock();
		$blockchainMock->staticExpects($this->once())
			->method("getClient")
			->will($this->returnValue($blockchainMock));
		// handle transaction: 100 from FROM1 to TO1
		$blockchainMock->expects($this->at(0))
			->method("__call")
			->with($this->equalTo("sendmany"), $this->equalTo(["FROM1", ["TO1" => 100], 1]))
			->will($this->returnValue("TXID1"));
		// handle transaction: 200 from FROM2 to TO1, 300 from FROM2 to TO2
		$blockchainMock->expects($this->at(1))
			->method("__call")
			->with($this->equalTo("sendmany"), $this->equalTo(["FROM2", ["TO1" => 200, "TO2" => 300], 1]))
			->will($this->returnValue("TXID2"));
		// handle transaction: 400 from FROM3 to TO2
		$blockchainMock->expects($this->at(2))
			->method("__call")
			->with($this->equalTo("sendmany"), $this->equalTo(["FROM3", ["TO2" => 400], 1]))
			->will($this->returnValue("TXID3"));

		ServiceLocator::bind("Blockchain", $blockchainMock);

		// mock PDO helper
		$helperMock = $this->getMock("PDOHelper");
		$helperMock->expects($this->once())
			->method("fetchAll")
			->will($this->returnValue($rows));
		// handle txid update for id 1
		$helperMock->expects($this->at(1))
			->method("update")
			->with(
				$this->equalTo(Payout::TABLE_NAME),
				$this->equalTo(["txid" => "TXID1", "cleared_on" => DATETIME]),
				$this->equalTo("id IN (:list)"),
				$this->equalTo(["list" => "1"])
			)
			->will($this->returnValue(true));
		// handle txid update for id 2 and 3
		$helperMock->expects($this->at(2))
			->method("update")
			->with(
				$this->equalTo(Payout::TABLE_NAME),
				$this->equalTo(["txid" => "TXID2", "cleared_on" => DATETIME]),
				$this->equalTo("id IN (:list)"),
				$this->equalTo(["list" => "2, 3"])
			)
			->will($this->returnValue(true));
		// handle txid update for id 4
		$helperMock->expects($this->at(3))
			->method("update")
			->with(
				$this->equalTo(Payout::TABLE_NAME),
				$this->equalTo(["txid" => "TXID3", "cleared_on" => DATETIME]),
				$this->equalTo("id IN (:list)"),
				$this->equalTo(["list" => "4"])
			)
			->will($this->returnValue(true));

		ServiceLocator::bind("PDOHelper", static function () use ($helperMock) {
			return $helperMock;
		});

		$processor = new Processor($currency);
		$out = $processor->send();
		$this->assertEquals(["TXID1", "TXID2", "TXID3"], $out);
	}

}