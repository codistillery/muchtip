<?php

require_once __DIR__ . "/../services/SourceJarService.php";

final class SourceJarServiceTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		$_POST = [];
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function post_400() {
		$service = new SourceJarService();

		// test with missing address parameter
		$_POST = ["currency" => "btc"];
		$out = $service->post(1);
		$this->assertEquals(400, $service->getHttpStatusCode());
		$this->assertFalse($out["ok"]);

		// test with mising currency parameter
		$_POST = ["address" => "17nMsWUzYiP3xSJTfJ8Vz9u6qZaNXdVdMw"];
		$out = $service->post(1);
		$this->assertEquals(400, $service->getHttpStatusCode());
		$this->assertFalse($out["ok"]);
	}

	/**
	 * @test
	 */
	public function post_404() {
		$this->markTestIncomplete("Need to mock Source::exists() returning false");
	}

	/**
	 * @test
	 */
	public function post_201() {
		$sourceId = 1;

		// mock jar
		$jar = new Jar();
		$jar->id = 1;
		$jar->sourceId = $sourceId;
		$jar->currency = "btc";
		$jar->jarAddress = "17nMsWUzYiP3xSJTfJ8Vz9u6qZaNXdVdMw";
		$jar->authorsAddress = "1KuHza8AKBVgLb6F6u1t17UcckmChE6gzQ";
		$jar->createdOn = DATETIME;
		$jar->expiresOn = DATETIME;

		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("get")
			->with($this->equalTo($sourceId))
			->will($this->returnValue($sourceMock));
		$sourceMock->expects($this->once())
			->method("exists")
			->will($this->returnValue(true));
		$sourceMock->expects($this->once())
			->method("generateJar")
			->with($this->equalTo($jar->currency), $this->equalTo($jar->authorsAddress))
			->will($this->returnValue($jar));
		ServiceLocator::bind("Source", $sourceMock);

		// mock post data
		$_POST = ["currency" => $jar->currency, "address" => $jar->authorsAddress];

		// test service
		$service = new SourceJarService();
		$out = $service->post($sourceId);
		$this->assertEquals(201, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$this->assertEquals($jar->id, $out["payload"]["id"]);
		$this->assertEquals(BASE_URL . "jar/{$jar->id}", $out["payload"]["jar"]);
		$this->assertEquals($jar->currency, $out["payload"]["currency"]);
		$this->assertEquals($jar->jarAddress, $out["payload"]["address"]);
	}

}