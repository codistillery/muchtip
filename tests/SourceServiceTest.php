<?php

require_once __DIR__ . "/../services/SourceService.php";

final class SourceServiceTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function index() {
		$source1 = new Source();
		$source1->id = 1;
		$source1->urlHash = md5(1);
		$source1->domain = "localhost.localdomain";
		$source2 = new Source();
		$source2->id = 2;
		$source2->urlHash = md5(2);
		$source2->domain = "somehost.somedomain";

		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("getAll")
			->will($this->returnValue([$source1, $source2]));
		ServiceLocator::bind("Source", $sourceMock);

		// test service
		$service = new SourceService();
		$out = $service->index();
		$this->assertTrue($out["ok"]);
		$expected = [
			$source1->urlHash => BASE_URL . "source/{$source1->id}",
			$source2->urlHash => BASE_URL . "source/{$source2->id}",
		];
		$this->assertEquals($expected, $out["payload"]);
	}

	/**
	 * @test
	 */
	public function get_200() {
		$source = new Source();
		$source->id = 1;
		$source->urlHash = md5(1);
		$source->domain = "localhost.localdomain";

		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("get")
			->will($this->returnValue($source));
		ServiceLocator::bind("Source", $sourceMock);

		// test service
		$service = new SourceService();
		$out = $service->get(1);
		$this->assertEquals(200, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$this->assertEquals($source->id, $out["payload"]["id"]);
		$this->assertEquals($source->urlHash, $out["payload"]["hash"]);
	}

	/**
	 * @test
	 */
	public function get_404() {
		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("get")
			->will($this->returnValue(new Source()));
		ServiceLocator::bind("Source", $sourceMock);

		// test service
		$service = new SourceService();
		$out = $service->get(1);
		$this->assertEquals(404, $service->getHttpStatusCode());
		$this->assertFalse($out["ok"]);
		$this->assertEquals([], $out["payload"]);
	}

	/**
	 * @test
	 */
	public function post_200() {
		$source = new Source();
		$source->id = 1;
		$source->urlHash = md5(1);
		$source->domain = "localhost.localdomain";

		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("get")
			->will($this->returnValue($source));
		ServiceLocator::bind("Source", $sourceMock);

		// test service
		$service = new SourceService();
		$out = $service->post(1);
		$this->assertEquals(200, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$this->assertEquals($source->id, $out["payload"]["id"]);
		$this->assertEquals($source->urlHash, $out["payload"]["hash"]);
	}

	/**
	 * @test
	 */
	public function post_201() {
		$source = new Source();
		$source->id = 1;
		$source->urlHash = md5(1);
		$source->domain = "localhost.localdomain";

		// mock source
		$sourceMock = $this->getMock("Source");
		$sourceMock->staticExpects($this->once())
			->method("get")
			->will($this->returnValue(new Source()));
		$sourceMock->staticExpects($this->once())
			->method("create")
			->will($this->returnValue($source));
		ServiceLocator::bind("Source", $sourceMock);

		// test service
		$service = new SourceService();
		$out = $service->post(1);
		$this->assertEquals(201, $service->getHttpStatusCode());
		$this->assertTrue($out["ok"]);
		$this->assertEquals($source->id, $out["payload"]["id"]);
		$this->assertEquals($source->urlHash, $out["payload"]["hash"]);
	}

}