<?php

final class SourceTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function create_ok() {
		$primaryKey = 2;
		$domain = "localhost.localdomain";
		$url = "http://$domain/source/" . DATETIME;

		// mock storage
		$storageMock = $this->getMockBuilder("Storage")
			->disableOriginalConstructor()
			->getMock();
		$storageMock->staticExpects($this->once())
			->method("getInstance")
			->will($this->returnValue($storageMock));
		$storageMock->expects($this->once())
			->method("save")
			->will($this->returnCallback(static function (Source $source) use ($primaryKey) {
				$source->id = $primaryKey;
				return $primaryKey;
			}));
		ServiceLocator::bind("Storage", $storageMock);

		// mock database helper
		$pdoHelperMock = $this->getMock("PDOHelper");
		$pdoHelperMock->expects($this->any())
			->method("insert")
			->with($this->equalTo("urls"), $this->equalTo(["source_id" => $primaryKey, "url" => $url]))
			->will($this->returnValue(true));
		ServiceLocator::bind("PDOHelper", $pdoHelperMock);

		// create source
		$source = Source::create($url);
		$this->assertEquals($primaryKey, $source->id);
		$this->assertEquals($domain, $source->domain);
		$this->assertEquals(md5($url), $source->urlHash);
	}

	/**
	 * @test
	 */
	public function generateJar() {
		$currency = "btc";
		$jarAddress = "1NhADEWuLvQXHjuvhY9QizDqBxYVZubJ6G";
		$authorsAddress = "1f9qtQEcCUhZcxpomrDXDw2u5esjXfcWR";
		$timeout = 10;
		$primaryKey = 1;

		// mock blockchain client
		$blockchainMock = $this->getMockBuilder("Blockchain")
			->disableOriginalConstructor()
			->getMock();
		$blockchainMock->staticExpects($this->once())
			->method("getClient")
			->with($this->equalTo($currency))
			->will($this->returnValue($blockchainMock));
		$blockchainMock->expects($this->once())
			->method("__call")
			->with($this->equalTo("getnewaddress"), $this->equalTo(["source_{$primaryKey}"]))
			->will($this->returnValue($jarAddress));
		ServiceLocator::bind("Blockchain", $blockchainMock);

		// mock storage
		$storageMock = $this->getMockBuilder("Storage")
			->disableOriginalConstructor()
			->getMock();
		$storageMock->staticExpects($this->once())
			->method("getInstance")
			->will($this->returnValue($storageMock));
		$storageMock->expects($this->once())
			->method("save")
			->will($this->returnCallback(static function (Jar $jar) use ($primaryKey) {
				$jar->id = $primaryKey;
				return $primaryKey;
			}));
		ServiceLocator::bind("Storage", $storageMock);
		
		$source = new Source();
		$source->id = 1;
		$source->urlHash = md5("1");

		$jar = $source->generateJar($currency, $authorsAddress, $timeout);
		$this->assertEquals($source->id, $jar->sourceId);
		$this->assertEquals($jarAddress, $jar->jarAddress);
		$this->assertEquals($authorsAddress, $jar->authorsAddress);
		$this->assertEquals($currency, $jar->currency);
		$this->assertEquals(strtotime($jar->createdOn) + $timeout, strtotime($jar->expiresOn));
	}

}