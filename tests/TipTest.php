<?php

final class TipTest extends PHPUnit_Framework_TestCase {

	public function setUp() {
		ServiceLocator::expunge();
	}

	/**
	 * @test
	 */
	public function receive() {
		$donorsAddress = "1LatestDonor";
		$txid = "txtxtxtxtxtxtx";
		$amount = 1000;
		$pastTipAmount1 = 7500;
		$pastTipAmount2 = 2500;
		$pastTipAmountTotal = $pastTipAmount1 + $pastTipAmount2;
		$expectedFeeShare = (int)(FEE_SHARE * $amount);
		$expectedAuthorsShare = (int)(AUTHORS_SHARE * ($amount - $expectedFeeShare));
		$expectedPastTipShare1 = (int)(($amount - $expectedFeeShare - $expectedAuthorsShare) * ($pastTipAmount1 / $pastTipAmountTotal));
		$expectedPastTipShare2 = (int)(($amount - $expectedFeeShare - $expectedAuthorsShare) * ($pastTipAmount2 / $pastTipAmountTotal));
		$expectedLeftovers = (int)($amount - $expectedFeeShare - $expectedAuthorsShare - $expectedPastTipShare1 - $expectedPastTipShare2);
		$tipPrimaryKey = 10;
/*
echo "Amount = $amount\n";
echo "Fee = $expectedFeeShare\n";
echo "Author = $expectedAuthorsShare\n";
echo "Past tip 1 = $expectedPastTipShare1\n";
echo "Past tip 2 = $expectedPastTipShare2\n";
echo "Leftovers = $expectedLeftovers\n";
*/
		// validate computation
		$this->assertEquals($amount, $expectedFeeShare + $expectedAuthorsShare + $expectedPastTipShare1 + $expectedPastTipShare2 + $expectedLeftovers);

		// mock jar
		$jar = new Jar();
		$jar->id = 1;
		$jar->sourceId = 5;
		$jar->authorsAddress = "1Author";
		$jar->jarAddress = "1Jar";
		$jar->currency = "btc";
		$jar->createdOn = DATETIME;
		$jar->expiresOn = DATETIME;

		// mock past tips
		$pastTip1 = new Tip();
		$pastTip1->currency = "btc";
		$pastTip1->amount = $pastTipAmount1;
		$pastTip1->donorsAddress = "PastTip1";
		$pastTip2 = new Tip();
		$pastTip2->currency = "btc";
		$pastTip2->amount = $pastTipAmount2;
		$pastTip2->donorsAddress = "PastTip2";
		$pastTips = [$pastTip1, $pastTip2];

		// mock storage
		$storageMock = $this->getMockBuilder("Storage")
			->disableOriginalConstructor()
			->getMock();
		$storageMock->staticExpects($this->exactly(7)) // save tip, select past tips, fee, author's, 2 x dividents, leftovers = 7
			->method("getInstance")
			->will($this->returnValue($storageMock));
		// save initial tip
		$storageMock->expects($this->at(0))
			->method("save")
			->will($this->returnCallback(static function (Tip $tip) use ($tipPrimaryKey) {
				$tip->id = $tipPrimaryKey;
				return $tipPrimaryKey;
			}));
		// return past tips
		$storageMock->expects($this->at(1))
			->method("loadInto")
			->will($this->returnValue($pastTips));
		// save fee payout
		$storageMock->expects($this->at(2))
			->method("save")
			->with($this->callback(static function (Payout $p) use ($amount, $expectedFeeShare) {
				return $p->type === Payout::TYPE_FEE and $p->amount === $expectedFeeShare;
			}))
			->will($this->returnValue(true));
		// save author's payout
		$storageMock->expects($this->at(3))
			->method("save")
			->with($this->callback(static function (Payout $p) use ($amount, $expectedAuthorsShare) {
				return $p->type === Payout::TYPE_AUTHOR and $p->amount === $expectedAuthorsShare;
			}))
			->will($this->returnValue(true));
		// save donors' payouts
		$storageMock->expects($this->at(4))
			->method("save")
			->with($this->callback(static function (Payout $p) use ($amount, $expectedPastTipShare1) {
				return $p->type === Payout::TYPE_DIVIDEND and $p->amount === $expectedPastTipShare1;
			}))
			->will($this->returnValue(true));
		$storageMock->expects($this->at(5))
			->method("save")
			->with($this->callback(static function (Payout $p) use ($amount, $expectedPastTipShare2) {
				return $p->type === Payout::TYPE_DIVIDEND and $p->amount === $expectedPastTipShare2;
			}))
			->will($this->returnValue(true));
		// save leftovers
		$storageMock->expects($this->at(6))
			->method("save")
			->with($this->callback(static function (Payout $p) use ($amount, $expectedLeftovers) {
				return $p->type === Payout::TYPE_LEFTOVERS and $p->amount === $expectedLeftovers;
			}))
			->will($this->returnValue(true));

		ServiceLocator::bind("Storage", $storageMock);

		// receive tip
		Tip::receive($jar, $amount, $donorsAddress, $txid);
	}

}